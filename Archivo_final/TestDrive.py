"""
Instituto Tecnologico de Costa Rica
Computer Engineering
Taller de Programacion

Turing Telemetry
Implementación del Test Drive
Proyecto 3, semestre 1
2019

Profesor: Milton Villegase Lemus
Autor: Jose Alejandro Chavarria Madriz

Restricciónes: Python3.7
Utiliza el módudo NodeMCU de wifiConnection

"""
#           _____________________________
# __________/BIBLIOTECAS
from tkinter import *  # Tk(), Label, Canvas, Photo
from threading import Thread  # p.start()
import threading  #
import os  # ruta = os.path.join('')
import time  # time.sleep(x)
from tkinter import messagebox  # AskYesNo ()
import tkinter.scrolledtext as tkscrolled
import PIL
from PIL import Image,ImageTk
import sys
##### Biblioteca para el Carro
from WiFiClient import NodeMCU #Para conectarse con el carrito. Autor Santiago Gamboa Ramírez
from rotation import rotate
sys.setrecursionlimit(6000) #Extiene el limite recursivo



def test_drive(indice, lista, main,matriz,logo):
#            __________________________________________________
# __________/Variables golbales para las funciones del vehiculo
    global luz_frontal, luz_tracera, dir, pwm, direc, mnsRecv, blvl, special, zero, light, rest, cont, destroy, abierto,parking
    direc = 0
    pwm = 700
    dir = 0
    luz_frontal = False
    luz_tracera = False
    mnsRecv = ''
    blvl = 0
    special = False
    zero = False
    light = False
    rest = False
    abierto = False
    cont = 0
    destroy = False
    parking = False
#           ____________________________
# __________/Ventana Principal
    root = Toplevel()
    root.title('Test Drive')
    root.minsize(1280, 720)
    root.resizable(width=NO, height=NO)
    root.overrideredirect(1)
    X=1280
    Y=720

    #           ______________________________
    # __________/Se crea un lienzo para objetos
    C_root = Canvas(root, width=1280, height=720, bg='white')
    C_root.place(x=0, y=0)


    #            ____________________________
    # __________/Funciónes para cargar imagenes
    # Tomada del codigo de Santiago Gamboa Ramírez
    def cargarImg(nombre):
        ruta = os.path.join('img', nombre)
        imagen = PhotoImage(file=ruta)
        return imagen

    def cargarImg2(nombre):
        ruta = os.path.join(nombre)
        imagen = PhotoImage(file=ruta)
        return imagen

    def cargarImg3(nombre):
        ruta = os.path.join('Images',nombre)
        imagen = PhotoImage(file=ruta)
        return imagen


    #            ____________________________
    # __________/Pantalla para el boton help
    def instructivo():
        global abierto
        
        if not abierto:
            abierto=True
            instruc = Toplevel()
            instruc.title("Controles")
            instruc.minsize(X // 2, Y // 2)
            instruc.resizable(width=NO, height=NO)
            instruc.overrideredirect(1)
            # Canvas de help
            C_help = Canvas(instruc, width=X//2, height=Y//2, bg='black')
            C_help.place(x=0, y=0)

            controles = cargarImg("controles.png")
            C_help.create_image(0,0, image=controles, anchor=NW, tags='ins', state=NORMAL)
            def salir():
                global abierto
                instruc.destroy()
                abierto=False
                
            Quit= Button(C_help, command=salir,text="Salir",font=("Steamwreck",20),fg="white",bg="black")
            Quit.place(x=X // 2-60,y=Y // 2-60)
            instruc.mainloop()
        
            

    #            _____________________________________
    # __________/Creando el cliente para NodeMCU
    # Tomada del codigo de Santiago Gamboa Ramírez
    myCar = NodeMCU()
    myCar.start()

    #Funcion tomada del codigo de Santiago Gamboa Ramírez
    def get_log():
        # Hilo que actualiza los Text cada vez que se agrega un nuevo mensaje al log de myCar
        global mnsRecv,destroy
        while not destroy:
            indice = 0
            while (myCar.loop):
                while (indice < len(myCar.log)):
                    mnsRecv = "{1}\n".format(indice, myCar.log[0][1])
                    print(mnsRecv)
                    myCar.log = []
                time.sleep(0.200)


    #        ______________________
    # _______/imagenes de la ventana

    #Fondo
    fondo = cargarImg("fondo2.gif")
    C_root.create_image(-10, -10, image=fondo, anchor=NW, tags='fondo')
    #Logo
    Logo = cargarImg3(logo)
    C_root.create_image(X // 2, 100, image=Logo, anchor=CENTER)

    #Luces frontales
    FrontL_on = cargarImg("frontL_on.gif")
    FrontL_off = cargarImg("frontL_off.gif")
    C_root.create_image(X/3 - 20, Y/2 - 70, image=FrontL_on, anchor=CENTER, tags='front_on', state=HIDDEN)
    C_root.create_image(X/3 - 20, Y/2 - 70, image=FrontL_off, anchor=CENTER, tags='front_off', state=NORMAL)

    #Luces traceras
    BackL_on = cargarImg("backL_on.gif")
    BackL_off = cargarImg("backL_off.gif")
    C_root.create_image(2*X/3 + 20, Y/2 - 70, image=BackL_on, anchor=CENTER, tags='back_on', state=HIDDEN)
    C_root.create_image(2*X/3 + 20, Y/2 - 70, image=BackL_off, anchor=CENTER, tags='back_off', state=NORMAL)

    C_root.create_image(50,200,image=BackL_on, anchor=CENTER, tags='busy_on', state=HIDDEN)
    C_root.create_image(50,200,image=BackL_off, anchor=CENTER, tags='busy_off', state=NORMAL)
    C_root.create_text(100,180,anchor=NW, text="Especial",font=("Steamwreck",40),tag="busy",fill= 'black' )

    #Luces direccioneles
    RightL_on = cargarImg("rightL_on.gif")
    RightL_off = cargarImg("rightL_off.gif")
    C_root.create_image(2*X/3 + 20, Y-70, image=RightL_on, anchor=CENTER, tags='rightL_on', state=HIDDEN)
    C_root.create_image(2*X/3 + 20, Y-70, image=RightL_off, anchor=CENTER, tags='rightL_off', state=NORMAL)

    C_root.create_image(X/3 - 20, Y-70, image=RightL_on, anchor=CENTER, tags='leftL_on', state=HIDDEN)
    C_root.create_image(X/3 - 20, Y-70, image=RightL_off, anchor=CENTER, tags='leftL_off', state=NORMAL)

    #Engranaje solar
    sol = cargarImg2("sol.png")
    C_root.create_image(X, 150, image=sol, anchor=E, tags='sol', state=NORMAL)

    #Indicador de Bateria
    battery = cargarImg("bat.png")
    C_root.create_image(X/7, 2*Y/3+50, image=battery, anchor=CENTER, tags='b', state=NORMAL)

    #Velocimetro
    rotate(650)
    velocimetro = cargarImg2('new_gauge.png')
    C_root.create_image(X/2, Y-250, image=velocimetro, anchor=CENTER, tags='gauge', state=NORMAL)

    #Acelerometro
    avance_1 = cargarImg("vf1.png")
    avance_2 = cargarImg("vf2.png")
    avance_3 = cargarImg("vf3.png")
    avance_4 = cargarImg("vf4.png")

    acel_off = cargarImg("v0.png")

    retro_1 = cargarImg("vr1.png")
    retro_2 = cargarImg("vr2.png")
    retro_3 = cargarImg("vr3.png")
    retro_4 = cargarImg("vr4.png")

    C_root.create_image(2*X/3 + 220, Y/2 +100, image=avance_1, anchor=CENTER, tags=('f','f1'), state=HIDDEN)
    C_root.create_image(2*X/3 + 220, Y/2 +100, image=avance_2, anchor=CENTER, tags=('f','f2'), state=HIDDEN)
    C_root.create_image(2*X/3 + 220, Y/2 +100, image=avance_3, anchor=CENTER, tags=('f','f3'), state=HIDDEN)
    C_root.create_image(2*X/3 + 220, Y/2 +100, image=avance_4, anchor=CENTER, tags=('f','f4'), state=HIDDEN)

    C_root.create_image(2*X/3 + 220, Y/2 +100, image=acel_off, anchor=CENTER, tags=('s'),state=NORMAL)

    C_root.create_image(2*X/3 + 220, Y/2 +100, image=retro_1, anchor=CENTER, tags=('r','r1'), state=HIDDEN)
    C_root.create_image(2*X/3 + 220, Y/2 +100, image=retro_2, anchor=CENTER, tags=('r','r2'), state=HIDDEN)
    C_root.create_image(2*X/3 + 220, Y/2 +100, image=retro_3, anchor=CENTER, tags=('r','r3'), state=HIDDEN)
    C_root.create_image(2*X/3 + 220, Y/2 +100, image=retro_4, anchor=CENTER, tags=('r','r4'), state=HIDDEN)



    #         _________________________________________
    # ________/Modulos para ejeccion de instrucciones

    def luz_frontal_func(event):
        global luz_frontal
        if luz_frontal:
            myCar.send("lf:0;")
            luz_frontal = False
            C_root.itemconfig('front_on', state=HIDDEN)
            C_root.itemconfig('front_off', state=NORMAL)
        elif not luz_frontal:
            myCar.send("lf:1;")
            luz_frontal = True
            C_root.itemconfig('front_off', state=HIDDEN)
            C_root.itemconfig('front_on', state=NORMAL)

    def luz_tracera_func(event):
        global luz_tracera
        if luz_tracera:
            myCar.send("lb:0;")
            luz_tracera = False
            C_root.itemconfig('back_on', state=HIDDEN)
            C_root.itemconfig('back_off', state=NORMAL)
        elif not luz_tracera:
            myCar.send("lb:1;")
            luz_tracera = True
            C_root.itemconfig('back_off', state=HIDDEN)
            C_root.itemconfig('back_on', state=NORMAL)


    def dir_right(event):
        global dir, direc
        if dir == 0:
            myCar.send("dir:1;")
            dir = 1
            direc = 1
        elif dir == 1:
            pass

    def dir_left(event):
        global dir, direc
        if dir == 0:
            myCar.send("dir:-1;")
            dir = -1
            direc = -1
        elif dir == -1:
            pass

    def dir_neutro(event):
        global dir, direc
        myCar.send("dir:0;")
        if direc == 1:
            myCar.send("lr:0;")
        elif direc == -1:
            myCar.send("ll:0;")
        dir = 0
        direc = 0


    def avanzar(event):
        global pwm
        pwm += 3
        print(pwm)
        pwm1 = str(pwm)
        instruccion = 'pwm:' + pwm1 + ';'
        print(instruccion)
        myCar.send('pwm:' + pwm1 + ';')

    def retroceder(event):
        global pwm,luz_tracera
        pwm = -abs(pwm)
        pwm -= 2
        print(pwm)
        pwm1 = str(pwm)
        instruccion = 'pwm:' + pwm1 + ';'
        print(instruccion)
        myCar.send('pwm:' + pwm1 + ';')
        if not luz_tracera:
            myCar.send("lb:1;")
            luz_tracera = True
            C_root.itemconfig('back_off', state=HIDDEN)
            C_root.itemconfig('back_on', state=NORMAL)

    def stop(event):
        global pwm,luz_tracera
        pwm = 700
        myCar.send('pwm:0;')
        def stop_aux(cont):
            if cont < 5:
                myCar.send('pwm:0;')
                stop_aux(cont+1)
            else:
                pass
        if luz_tracera:
            myCar.send("lb:0;")
            luz_tracera = False
            C_root.itemconfig('back_on', state=HIDDEN)
            C_root.itemconfig('back_off', state=NORMAL)
        stop_aux(0)


    def nitro(event):
        global pwm
        pwm=1024
        myCar.send('pwm:1024;')
        print('Fast n Fast')


    def park(event):
        global parking
        if parking:
            parking=False
        else:
            parking=True

    # Funcion para el control artificial de la luz
    """def luna(event):
        global light
        if light:
            light=False
            print("no luz")
        else:
            light=True
            print("luz")"""

    #         ________________________
    # _______/Threads de la ventana
    def veloc_init():
        global destroy,pwm
        try:
            time.sleep(0.2)
            if not destroy:
                veloc(pwm)
        except:
            pass
    def veloc(current_pwm):
        global pwm,dir,zero
        if pwm != 700 and abs(pwm)<1024:
            zero=False
            rotate(abs(current_pwm))
            velocimetro2 = cargarImg2('new_gauge.png')
            C_root.delete('gauge')
            C_root.create_image(X/2, Y-250,image=velocimetro2, anchor=CENTER, tags='gauge', state=NORMAL)
            if pwm > 700 and pwm < 800:
                C_root.itemconfig('f', state=HIDDEN)
                C_root.itemconfig('r', state=HIDDEN)
                C_root.itemconfig('s', state=HIDDEN)
                C_root.itemconfig('f1',state=NORMAL)

            elif pwm >=800 and pwm < 900:
                C_root.itemconfig('f', state=HIDDEN)
                C_root.itemconfig('r', state=HIDDEN)
                C_root.itemconfig('s', state=HIDDEN)
                C_root.itemconfig('f2',state=NORMAL)

            elif pwm >= 900:
                C_root.itemconfig('f', state=HIDDEN)
                C_root.itemconfig('r', state=HIDDEN)
                C_root.itemconfig('s', state=HIDDEN)
                C_root.itemconfig('f3', state=NORMAL)

            elif pwm < -700 and pwm > -800:
                C_root.itemconfig('f', state=HIDDEN)
                C_root.itemconfig('r', state=HIDDEN)
                C_root.itemconfig('s', state=HIDDEN)
                C_root.itemconfig('r1', state=NORMAL)

            elif pwm <= -800 and pwm > -900:
                C_root.itemconfig('f', state=HIDDEN)
                C_root.itemconfig('r', state=HIDDEN)
                C_root.itemconfig('s', state=HIDDEN)
                C_root.itemconfig('r2', state=NORMAL)

            elif pwm <= -900:
                C_root.itemconfig('f', state=HIDDEN)
                C_root.itemconfig('r', state=HIDDEN)
                C_root.itemconfig('s', state=HIDDEN)
                C_root.itemconfig('r3', state=NORMAL)

        elif pwm == 700 and not zero:
            zero=True
            rotate(650)
            velocimetro2 = cargarImg2('new_gauge.png')
            C_root.create_image(X/2, Y-250, image=velocimetro2, anchor=CENTER, tags='gauge', state=NORMAL)
            C_root.itemconfig('f', state=HIDDEN)
            C_root.itemconfig('r', state=HIDDEN)
            C_root.itemconfig('s', state=NORMAL)

        elif abs(pwm) >=1024:
            zero = False
            rotate(1024)
            print("tops")
            velocimetro2 = cargarImg2('new_gauge.png')
            C_root.create_image(X/2, Y-250, image=velocimetro2, anchor=CENTER, tags='gauge', state=NORMAL)
            if pwm >= 1024:
                C_root.itemconfig('s', state=HIDDEN)
                C_root.itemconfig('r', state=HIDDEN)
                C_root.itemconfig('f', state=HIDDEN)
                C_root.itemconfig('f4', state=NORMAL)

            elif pwm <= -1024:
                C_root.itemconfig('s', state=HIDDEN)
                C_root.itemconfig('f', state=HIDDEN)
                C_root.itemconfig('r', state=HIDDEN)
                C_root.itemconfig('r4', state=NORMAL)

        veloc_init()
        root.mainloop()

    C_root.create_text(X-90, 2*Y/3, anchor=CENTER, text="Vial de\naceleracion", font=("Steamwreck", 35), tag="luz", fill='black')

    def parpadeo_init():
        global destroy
        try:
            if not destroy:
                parpadeo()
        except:
            pass
    def parpadeo():
        global direc,parking
        while direc == 1:
            myCar.send('lr:1;')
            C_root.itemconfig('rightL_on', state=NORMAL)
            C_root.itemconfig('rightL_off', state=HIDDEN)
            time.sleep(0.5)
            myCar.send('lr:0;')
            C_root.itemconfig('rightL_on', state=HIDDEN)
            C_root.itemconfig('rightL_off', state=NORMAL)
            time.sleep(0.5)
        while dir == -1:
            myCar.send('ll:1;')
            C_root.itemconfig('leftL_on', state=NORMAL)
            C_root.itemconfig('leftL_off', state=HIDDEN)
            time.sleep(0.5)
            myCar.send('ll:0;')
            C_root.itemconfig('leftL_on', state=HIDDEN)
            C_root.itemconfig('leftL_off', state=NORMAL)
            time.sleep(0.5)
        while parking:
            myCar.send('lr:1;')
            myCar.send('ll:1;')
            C_root.itemconfig('rightL_on', state=NORMAL)
            C_root.itemconfig('rightL_off', state=HIDDEN)
            C_root.itemconfig('leftL_on', state=NORMAL)
            C_root.itemconfig('leftL_off', state=HIDDEN)
            time.sleep(0.5)
            myCar.send('lr:0;')
            myCar.send('ll:0;')
            C_root.itemconfig('rightL_on', state=HIDDEN)
            C_root.itemconfig('rightL_off', state=NORMAL)
            myCar.send('ll:0;')
            C_root.itemconfig('leftL_on', state=HIDDEN)
            C_root.itemconfig('leftL_off', state=NORMAL)
            time.sleep(0.5)

        time.sleep(1)
        parpadeo_init()


    def ard_especial(event,mov):
        global special
        def circle():
            global special
            myCar.send('Circle:1;')
            print('circulo')
            time.sleep(5)
            special = False
            teclas()
            C_root.itemconfig('busy_on', state=HIDDEN)
            C_root.itemconfig('busy_off', state=NORMAL)
        def infinite():
            global special
            myCar.send('Infinite:1;')
            print('infinito')
            time.sleep(5)
            special = False
            teclas()
            C_root.itemconfig('busy_on', state=HIDDEN)
            C_root.itemconfig('busy_off', state=NORMAL)
        def  zigzag():
            global special
            myCar.send('ZigZag:1;')
            print('zigzag')
            time.sleep(5)
            special = False
            teclas()
            C_root.itemconfig('busy_on', state=HIDDEN)
            C_root.itemconfig('busy_off', state=NORMAL)
        def triquetra():
            global special
            myCar.send('Especial:1;')
            print('triquetra')
            time.sleep(5)
            special=False
            teclas()
            C_root.itemconfig('busy_on', state=HIDDEN)
            C_root.itemconfig('busy_off', state=NORMAL)
        if not special:
            special=True
            teclas_off()
            C_root.itemconfig('busy_off', state=HIDDEN)
            C_root.itemconfig('busy_on', state=NORMAL)
            if mov==0:
                esp=Thread(target=circle,args=())
                esp.start()

            elif mov==1:
                esp = Thread(target=infinite, args=())
                esp.start()

            elif mov==2:
                esp = Thread(target=zigzag, args=())
                esp.start()

            elif mov==3:
                esp = Thread(target=triquetra, args=())
                esp.start()

        else:
            print('busy')


    def sense_init():
        global destroy
        try:
            time.sleep(2)
            if not destroy:
                sense()
        except:
            pass
    def sense():
        global mnsRecv, blvl, light
        myCar.send('sense;')
        current_mns = str(mnsRecv)
        if current_mns != '':
            if current_mns[0] == 'b':
                try:
                    if isinstance ((int(current_mns[7])),int):
                        blvl = int(current_mns[5] + current_mns[6] + current_mns[7])
                        print(str(blvl))
                        if current_mns[13] == '0':
                            print('no luz')
                            light = False
                        elif current_mns[13] == '1':
                            print('luz')
                            light = True
                except:
                    blvl = int(current_mns[5] + current_mns[6])
                    print(str(blvl))
                    if current_mns[12] == '0':
                        print('no luz')
                        light = False
                    elif current_mns[12] == '1':
                        print('luz')
                        light = True
        bateria_aux()
        sense_init()
        root.mainloop()

    #Funciones para la luz del ambiente
    def sol_recovery():
        image = Image.open('sol_new.png')
        filename = "sol.png"
        image.save(filename)

    def lux_init():
        global destroy
        try:
            if not destroy:
                time.sleep(0.05)
                lux()
        except:
            pass
    def lux():
        global light,rest,cont
        if light:
            rest=False
            image = Image.open('sol.png')
            # rotate 10 degrees counter-clockwise
            imRotate = image.rotate(10,resample=PIL.Image.BICUBIC)
            filename = "sol.png"
            imRotate.save(filename)
            sol2 = cargarImg2('sol.png')
            C_root.delete('sol')
            C_root.create_image(X, 150, image=sol2, anchor=E, tags='sol', state=NORMAL)
            cont+=1
            if cont == 18:
                sol_recovery()
                sol2 = cargarImg2('sol.png')
                C_root.delete('sol')
                C_root.create_image(X, 150, image=sol2, anchor=E, tags='sol', state=NORMAL)
                cont=0
        else:
            if not rest:
                rest = True
                sol_recovery()
                sol2 = cargarImg2('sol.png')
                C_root.delete('sol')
                C_root.create_image(X, 150, image=sol2, anchor=E, tags='sol', state=NORMAL)
                cont=0
        time.sleep(0.6)
        lux_init()
        root.mainloop()
    C_root.create_text(X-350, 85,anchor=CENTER, text="Engrane\nsolar",font=("Steamwreck",40),tag="luz",fill= 'black' )

    #Funcion para modificar el texto de la bateria
    def bateria_aux():
        global blvl
        bateria = str(blvl)+"%"
        C_root.delete("bat")
        C_root.create_text(X / 7, 2*Y/3+50, anchor=CENTER, text=bateria, font=("Steamwreck", 40), tag="bat", fill='blue')
    C_root.create_text(X / 7, 2 * Y / 3 + 50, anchor=CENTER, text="0%", font=("Steamwreck", 40), tag="bat", fill='blue')

    #Funcion pricipal para moviminetos especiales
    def mov_esp(indice, event):
        global special
        if not special:
            special=True
            C_root.itemconfig('busy_off', state=HIDDEN)
            C_root.itemconfig('busy_on', state=NORMAL)
            if indice == 0:
                mov_0(event)
            elif indice == 1:
                mov_1(event)
            elif indice == 2:
                mov_2(event)
            elif indice == 3:
                mov_3(event)
            elif indice == 4:
                mov_4(event)
            elif indice == 5:
                mov_5(event)
            elif indice == 6:
                mov_6(event)
            elif indice == 7:
                mov_7(event)
            elif indice == 8:
                mov_8(event)
            elif indice == 9:
                mov_9(event)

    #Movimeintos especiales por piloto
    def mov_0(event):
        tiempo = 0
        global special
        while tiempo < 5:
            dir_neutro(event)
            luz_frontal_func(event)
            luz_tracera_func(event)
            dir_right(event)
            time.sleep(0.5)
            dir_neutro(event)
            luz_frontal_func(event)
            luz_tracera_func(event)
            dir_left(event)
            time.sleep(0.5)
            tiempo += 1
        dir_neutro(event)
        teclas()
        special=False
        C_root.itemconfig('busy_on', state=HIDDEN)
        C_root.itemconfig('busy_off', state=NORMAL)

    def mov_1(event):
        tiempo = 0
        global special
        while tiempo < 5:
            dir_neutro(event)
            luz_frontal_func(event)
            avanzar(event)
            time.sleep(2)
            stop(event)
            dir_left(event)
            time.sleep(1)
            dir_right(event)
            time.sleep(1)
            retroceder(event)
            time.sleep(2)
            tiempo += 5
        dir_neutro(event)
        stop(event)
        teclas()
        special=False
        C_root.itemconfig('busy_on', state=HIDDEN)
        C_root.itemconfig('busy_off', state=NORMAL)

    def mov_2(event):
        tiempo = 0
        global special
        while tiempo < 4:
            dir_neutro(event)
            luz_tracera_func(event)
            dir_right(event)
            avanzar(event)
            time.sleep(1)
            dir_neutro(event)
            luz_tracera_func(event)
            dir_left(event)
            retroceder(event)
            time.sleep(1)
            tiempo += 1
        dir_neutro(event)
        stop(event)
        teclas()
        special=False
        C_root.itemconfig('busy_on', state=HIDDEN)
        C_root.itemconfig('busy_off', state=NORMAL)

    def mov_3(event):
        tiempo = 0
        global special
        while tiempo < 6:
            dir_right(event)
            avanzar(event)
            time.sleep(2)
            tiempo += 2
            retroceder(event)
            time.sleep(2)
            tiempo += 2
        dir_neutro(event)
        stop(event)
        teclas()
        special=False
        C_root.itemconfig('busy_on', state=HIDDEN)
        C_root.itemconfig('busy_off', state=NORMAL)

    def mov_4(event):
        tiempo = 0
        global special
        while tiempo < 4:
            luz_tracera_func(event)
            luz_frontal_func(event)
            time.sleep(0.3)
            luz_tracera_func(event)
            luz_frontal_func(event)
            time.sleep(0.3)
            luz_tracera_func(event)
            luz_frontal_func(event)
            tiempo += 0.6
        avanzar(event)
        time.sleep(1)
        stop(event)
        dir_neutro(event)
        teclas()
        special=False
        C_root.itemconfig('busy_on', state=HIDDEN)
        C_root.itemconfig('busy_off', state=NORMAL)

    def mov_5(event):
        tiempo = 0
        global special
        while tiempo < 6:
            dir_neutro(event)
            time.sleep(0.5)
            dir_right(event)
            dir_neutro(event)
            avanzar(event)
            time.sleep(1)
            dir_left(event)
            time.sleep(0.5)
            dir_neutro(event)
            dir_right(event)
            time.sleep(0.5)
            dir_neutro(event)
            retroceder(event)
            time.sleep(1)
            tiempo += 2
        dir_neutro(event)
        stop(event)
        teclas()
        special=False
        C_root.itemconfig('busy_on', state=HIDDEN)
        C_root.itemconfig('busy_off', state=NORMAL)

    def mov_6(event):
        global special
        avanzar(event)
        luz_frontal_func(event)
        time.sleep(2)
        stop(event)
        luz_frontal_func(event)
        retroceder(event)
        luz_tracera_func(event)
        time.sleep(2)
        stop(event)
        luz_tracera_func(event)
        dir_right(event)
        time.sleep(0.5)
        dir_neutro(event)
        dir_left(event)
        time.sleep(0.5)
        dir_neutro(event)
        teclas()
        special=False
        C_root.itemconfig('busy_on', state=HIDDEN)
        C_root.itemconfig('busy_off', state=NORMAL)

    def mov_7(event):
        global special
        dir_right(event)
        avanzar(event)
        luz_frontal_func(event)
        time.sleep(3)
        dir_neutro(event)
        stop(event)
        luz_frontal_func(event)
        time.sleep(0.5)
        luz_frontal_func(event)
        luz_tracera_func(event)
        time.sleep(0.5)
        luz_frontal_func(event)
        luz_tracera_func(event)
        time.sleep(0.5)
        retroceder(event)
        time.sleep(1)
        stop(event)
        teclas()
        special=False
        C_root.itemconfig('busy_on', state=HIDDEN)
        C_root.itemconfig('busy_off', state=NORMAL)

    def mov_8(event):
        global special
        luz_tracera_func(event)
        time.sleep(0.5)
        luz_tracera_func(event)
        time.sleep(0.5)
        luz_tracera_func(event)
        dir_right(event)
        retroceder(event)
        time.sleep(0.5)
        dir_neutro(event)
        dir_left(event)
        time.sleep(0.5)
        dir_neutro(event)
        dir_right(event)
        time.sleep(0.5)
        dir_neutro(event)
        dir_left(event)
        time.sleep(0.5)
        dir_neutro(event)
        luz_tracera_func(event)
        stop(event)
        teclas()
        special=False
        C_root.itemconfig('busy_on', state=HIDDEN)
        C_root.itemconfig('busy_off', state=NORMAL)

    def mov_9(event):
        global special
        tiempo=0
        while tiempo<3:
            avanzar(event)
            luz_frontal_func(event)
            luz_tracera_func(event)
            time.sleep(0.5)
            luz_frontal_func(event)
            luz_tracera_func(event)
            time.sleep(0.5)
            tiempo+=1
        dir_left(event)
        retroceder(event)
        time.sleep(1)
        stop(event)
        dir_neutro(event)
        teclas()
        special=False
        C_root.itemconfig('busy_on', state=HIDDEN)
        C_root.itemconfig('busy_off', state=NORMAL)


    def piloto(lista):
        print (lista)
        img_piloto = cargarImg3(lista[2])
        C_root.create_image(30, 15, image=img_piloto, anchor=NW, tags='piloto', state=NORMAL)
        C_root.create_text(240, 50, text=lista[3], font=("Steamwreck", 40))
        C_root.create_text(240, 100, text=lista[5], font=("Steamwreck", 40))
        root.mainloop()

    #        _____________________
    #_______/Inicio de los threads

    p = Thread(target=get_log)
    p.start()

    direccionales = Thread(target=parpadeo_init, args=())
    direccionales.start()

    sense_T = Thread(target=(sense_init), args=())
    sense_T.start()

    def speedometer_thread():
        speedometer = Thread(target=veloc_init,args=())
        speedometer.start()
    speedometer_thread()

    def lux_thread():
        sol = Thread(target=lux_init,args=())
        sol.start()
    lux_thread()

    def especial(event):
        teclas_off()
        ind = indice
        especiales = Thread(target=mov_esp, args=(ind, event))
        especiales.start()

    #         _____________________
    # _______/Funciones que se ejecutan al cerrar la ventana

    def cerrar():
        global destroy,special,blvl
        if not special:
            destroy = True
            time.sleep(0.5)
            sol_recovery()
            root.destroy()
            main.deiconify()
            reescribir(blvl,"Bateria.txt")
            if blvl < 60:
                messagebox.showinfo("Aviso!", "El nivel de bateria es bajo.\nSe ha dehabilitado el vehiculo")

            rendimiento(indice, matriz)
        else:
            print('busy')

    def cerrar_esc(event):
        cerrar()

    def reescribir(Dato, Texto):
        """
        Reescribe la matriz que le entra al documento
        Entradas: MatrizPilotos, es la matriz que se va a escribir y Texto es en el arhivo que se va a escribir
        Restricciones: Texto debe ser "Pilotos.txt" o "Carros.txt"
        Salida: Se reescribe la nueva matriz en el documento
        """
        Archivo = open(Texto, "r+")
        Archivo.seek(0)
        Archivo.truncate()
        Archivo.write(str(Dato))
        Archivo.close()

    def update_pilot(Piloto,Cambio,Matriz,desem):
        Mat = Matriz
        Mat[Piloto][7] += 1
        if Cambio == "gano":
            Mat[Piloto][8] += 1
        elif Cambio == "casigano":
            Mat[Piloto][9] +=1
        elif Cambio == "abandono":
            Mat[Piloto][10] +=1
        elif Cambio == "perdio":
            pass
        Mat[Piloto][0] = round(((Mat[Piloto][8] + Mat[Piloto][9]) / (Mat[Piloto][7] - Mat[Piloto][10])) * 100)
        Mat[Piloto][1] = round((Mat[Piloto][8] / (Mat[Piloto][7] - Mat[Piloto][10])) * 100)
        reescribir(Mat, "Pilotos.txt")
        desem.destroy()

    #Ventana de rendikmiento del piloto
    def rendimiento(Piloto,matriz):
        des = Toplevel()
        des.title('Rendimiento del Piloto')
        des.minsize(1280 // 2, 720 // 2)
        des.resizable(width=NO, height=NO)
        des.overrideredirect(0)

        C_des = Canvas(des, width=1280 // 2, height=720 // 2, bg='white')
        C_des.place(x=0, y=0)

        FondoR = cargarImg("liz.png")
        C_des.create_image(-80, 0, image=FondoR, anchor=NW, tags='result', state=NORMAL)

        driver=str(matriz[Piloto][3])
        C_des.create_text(150,20, text=("rendimiento de: "+driver) , font=("Steamwreck", 35))

        Bgano = Button(C_des, command=lambda: update_pilot(Piloto, "gano",matriz,des), text="  Ganó  ", font=("Steamwreck", 25),
                       fg="white", bg="#332904")
        Bgano.place(relx=0.15, rely=0.35,anchor=CENTER)
        Bcasigano = Button(C_des, command=lambda: update_pilot(Piloto, "casigano",matriz,des), text=" Podio ",
                           font=("Steamwreck", 25), fg="white", bg="#332904")
        Bcasigano.place(relx=0.45, rely=0.35,anchor=CENTER)
        Babandono = Button(C_des, command=lambda: update_pilot(Piloto, "abandono",matriz,des), text="Abandono",
                           font=("Steamwreck", 25), fg="white", bg="#332904")
        Babandono.place(relx=0.15, rely=0.75,anchor=CENTER)
        Bperdio = Button(C_des, command=lambda: update_pilot(Piloto, "perdio",matriz,des), text=" Perdió ", font=("Steamwreck", 25),
                         fg="white", bg="#332904")
        Bperdio.place(relx=0.45, rely=0.75,anchor=CENTER)

        des.mainloop()

    #           ____________________________
    # __________/Teclas para el control del vehiculo
    def teclas():
        root.bind('q', luz_frontal_func)
        root.bind('e', luz_tracera_func)

        root.bind('<KeyPress-d>', dir_right)
        root.bind('<KeyRelease-d>', dir_neutro)

        root.bind('<KeyPress-a>', dir_left)
        root.bind('<KeyRelease-a>', dir_neutro)

        root.bind('<KeyPress-w>', avanzar)
        root.bind('<KeyRelease-w>', stop)

        root.bind('<KeyPress-s>', retroceder)
        root.bind('<KeyRelease-s>', stop)

        root.bind('<KeyPress-space>', especial)
        #root.bind('p',luna)
        root.bind('p',park)
    root.bind('c',lambda event: ard_especial(event, 0))
    root.bind('i', lambda event: ard_especial(event, 1))
    root.bind('z', lambda event: ard_especial(event, 2))
    root.bind('t', lambda event: ard_especial(event, 3))
    root.bind("<Escape>",cerrar_esc)
    root.bind('n',nitro)
    teclas()

    def teclas_off():
        root.unbind('q')
        root.unbind('e')
        root.unbind('<KeyPress-d>')
        root.unbind('<KeyRelease-d>')
        root.unbind('<KeyPress-a>')
        root.unbind('<KeyRelease-a>')
        root.unbind('<KeyPress-w>')
        root.unbind('<KeyRelease-w>')
        root.unbind('<KeyPress-s>')
        root.unbind('<KeyRelease-s>')
        root.unbind('<KeyPress-space>')

    #           ____________________________
    # __________/Botones de ventana principal
    Btn_help = Button(C_root, command=instructivo, text="Help",font=("Steamwreck",25), fg="white",bg="black")
    Btn_help.config(height=1, width=10)
    Btn_help.place(relx=0.98, rely=0.95, anchor=SE)
    Btn_cerrar = Button(C_root, command=cerrar, text="Finalizar", font=("Steamwreck", 25), fg="white", bg="black")
    Btn_cerrar.config(height=1, width=10)
    Btn_cerrar.place(relx=0.9, rely=0.95, anchor=SE)


    piloto(lista) #Llamada a la funcion que indentifica el piloto
    root.mainloop()

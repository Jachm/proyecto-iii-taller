About="""Instituto Tecnólogico de Costa Rica
Ingeniería en Computadores
Taller de Programación
Grupo 2
Telemetria Escuderia Turing
Autores:
Jose Alejandro Chavarría Madriz
2019067306
Natalia González Bermúdez
2019165109
Profesor:
Milton Villegas Lemus
País de Producción: Costa Rica
Fecha de emision: 5/06/2019
Versión 1.0
"""

#--------------------BIBLIOTECAS---------------------
from tkinter import * #Biblioteca para la interfaz gráfica
from tkinter import messagebox#Para mensajes de error o avisos
import os #Para obtener la ruta de algún archivo
import ast #Para obtener los datos del documento de texto y que no sea un string
import TestDrive

#------------------VARIABLES GLOBALES----------
global Editando, Piloto
Piloto=0#Variable global qu guarda el índice del piloto seleccionado para usarlo de indice y mostrar los datos en la pantalla de testdrive
Editando=False#Variable global para que no se puedan abrir variar ventanas de edición al mismo tiempo


#--------------FUNCION DE CARGA DE IMÁGENES------------------
def loadImg(Nombre):
    ruta=os.path.join("Images",Nombre)
    Imagen= PhotoImage(file=ruta)
    return Imagen


#_____________________________
#Inicio de ventana principal  \___________________________________________________________________________________________________________________
main=Tk()
main.title("Turing Telemetry")
main.minsize(1280,720)
#--Canvas principal
C_main=Canvas(main, width=1280,height=720)
C_main.place(x=0,y=0)

#Imagen para el fondo
Mainbck=loadImg("Mainbck.gif")
C_main.create_image(0,0,image=Mainbck,anchor=NW)



#_________________________________/Inicio de ventana de edicion de pilotos\_____________________________________________

def ventana_posiciones(Pag):
    """
    Función que crea la ventana de posiciones de los pilotos, recibe el número de páginas por que son 10 pilotos
    y muestra 5 en cada página, tiene un botón para volver a la pantalla principal, 4 botones para ordenar los
    pilotos dependiendo del que se seleccione y un boton para ir a la ventana que muestra los vehículos
    """
    main.withdraw()
    posiciones=Toplevel()
    posiciones.title("Tabla de posiciones")
    posiciones.minsize(1280,720)
    #Canvas de posiciones
    C_posiciones=Canvas(posiciones, width=1280, height=720, bg="red")
    C_posiciones.place(x=0,y=0)

    #Background de la primera página
    Pilotosbck= loadImg("Pilotosbck.gif")
    C_posiciones.create_image(0,0, image=Pilotosbck, anchor=NW,state=HIDDEN, tags="Pagina1")
    #Background de la segunda página
    Pilotosbck2= loadImg("Pilotosbck2.gif")
    C_posiciones.create_image(0,0, image=Pilotosbck2, anchor=NW,state=HIDDEN, tags="Pagina2")

    #Imagenes de los pilotos que aparecen en la pagina 1
    Piloto1=loadImg(obtener_pilotos()[0][2])
    C_posiciones.create_image(85,125, image=Piloto1, anchor=NW,state=HIDDEN,tags="Pagina1")
    Piloto2=loadImg(obtener_pilotos()[1][2])
    C_posiciones.create_image(85,250, image=Piloto2, anchor=NW,state=HIDDEN,tags="Pagina1")
    Piloto3=loadImg(obtener_pilotos()[2][2])
    C_posiciones.create_image(85,370, image=Piloto3, anchor=NW,state=HIDDEN,tags="Pagina1")
    Piloto4=loadImg(obtener_pilotos()[3][2])
    C_posiciones.create_image(85,490, image=Piloto4, anchor=NW,state=HIDDEN,tags="Pagina1")
    Piloto5=loadImg(obtener_pilotos()[4][2])
    C_posiciones.create_image(85,610, image=Piloto5, anchor=NW,state=HIDDEN,tags="Pagina1")
    #Imagenes de los pilotos que aparecen en la pagina 2
    Piloto6=loadImg(obtener_pilotos()[5][2])
    C_posiciones.create_image(85,125, image=Piloto6, anchor=NW,state=HIDDEN,tags="Pagina2")
    Piloto7=loadImg(obtener_pilotos()[6][2])
    C_posiciones.create_image(85,250, image=Piloto7, anchor=NW,state=HIDDEN,tags="Pagina2")
    Piloto8=loadImg(obtener_pilotos()[7][2])
    C_posiciones.create_image(85,370, image=Piloto8, anchor=NW,state=HIDDEN,tags="Pagina2")
    Piloto9=loadImg(obtener_pilotos()[8][2])
    C_posiciones.create_image(85,490, image=Piloto9, anchor=NW,state=HIDDEN,tags="Pagina2")
    Piloto10=loadImg(obtener_pilotos()[9][2])
    C_posiciones.create_image(85,610, image=Piloto10, anchor=NW,state=HIDDEN,tags="Pagina2")
    
    #P es el índice que se manda a la función de editar, como son solo 5 botones si se está en la página 2
    #el índice debe empezar en 5 y dependiendo de la página actual también cambia el color del botón
    if Pag==1:
        P=0
        ColorPag1="#332904"
        ColorPag2="black"
    if Pag==2:
        P=5
        ColorPag1="black"
        ColorPag2="#332904"
        
    def place_pilots(Matriz,Accion,Pag):
        """
        Función que coloca a los pilotos es las tablas de posiciones
        Entradas: La matriz a colocar, en este caso la de los pilotos
                  La acción, es decir si "write" escribe a los pilotos o "destroy" destruye la ventana para escribir algo nuevo
                  La página en que se está, con esto se decide el rango de la matriz, es decir de pilotos que se escribe
        Restricciones: Debe entrar una matriz, la acción puede ser "write" o "destroy" y la página solo puede se 1 o 2
        Salida: Se posicionan los datos de los pilotos en la ventana
        """
        #Dependiendo de la pagina, las imágenes de los pilotos y el rango de información que se tiene que posicionar
        if Pag==1:
            C_posiciones.itemconfig("Pagina2",state=HIDDEN)
            C_posiciones.itemconfig("Pagina1",state=NORMAL)
            Pilotos=Matriz[0:5]
        if Pag==2:
            C_posiciones.itemconfig("Pagina1",state=HIDDEN)
            C_posiciones.itemconfig("Pagina2",state=NORMAL)
            Pilotos=Matriz[5:10]
        #Posiciona a los pilotos, define "y" dependiendo del índice la posición en que van a estar
        if Accion=="write":
            Posy=155
            for Pilot in Pilotos:
                Posx=120
                for Ind in range(0,8):
                    if Ind==0:
                        Posx=1020
                    if Ind==1:
                        Posx=1170
                    if Ind==2:
                        Posx=-400
                    if Ind==3:
                        Posx=255
                    if Ind==4:
                        Posx=440
                    if Ind==5:
                        Posx=540
                    if Ind==6:
                        Posx=660
                    if Ind==7:
                        Posx=850
                        
                    L_Pilotos=Label(C_posiciones,text=str(Pilot[Ind]),font=("Steamwreck",30),fg="white",bg="black")
                    L_Pilotos.place(x=Posx,y=Posy)
                Posy+=120
        #Acción para "refrescar" la pantalla de posiciones cada vez que se le hace un cambio a la matriz
        if Accion=="destroy":
            posiciones.destroy()
            ventana_posiciones(Pag)
    #FUNCIONES PARA OBTENER Y EDITAR LOS DATOS DE LOS PILOTOS
    def orden_pilotos(Matriz,Orden,Pag):
        """
        Función que ordena a los pilotos dependiendo de como se requiera llamando a la función orden_pilotos_aux
        Entradas: Una matriz,con las datos de los pilotos, el orden respectivo y la página que hay que destruir y escribir para refrescar
        Restricciones: El primer dato de cada elemento de la matriz es el RGP y el segundo el REP, la página puede ser 1 o 2 y el orden puede ser:
                                -"RGPA":Ordenar por rendimiento global del piloto de forma ascendente
                                -"RGPD":Ordenar por rendimiento global del piloto de forma descendente
                                -"REPA":Ordenar por rendimiento específico del piloto de forma ascendente
                                -"REPD":Ordenar por rendimiento específico del piloto de forma descendente
        Salidas: Reescribe la matriz ordenada en el documento, destruye la página y la vuelve a crear con el orden correcto
        """
        reescribir(orden_pilotos_aux(Matriz,0,0,len(Matriz),False,Orden),"Pilotos.txt")
        place_pilots(obtener_pilotos(),"destroy",Pag)
        place_pilots(obtener_pilotos(),"write",Pag)
        
    def orden_pilotos_aux(Matriz,i,j,n,Swap,Orden):
        """
        Ordena los datos de la matriz según el orden que se le asigne por medio del algoritmo de ordenamiento de burbuja, es la función auxiliar
        de la funcion orden_pilotos
        """
        if j==n-i-1:
            if Swap:
                return orden_pilotos_aux(Matriz,i+1,0,n,False,Orden)
            else:
                return Matriz
        #Define el índice que toma como referencia en la matriz
        if Orden[1]=="G":
            a=0
        if Orden[1]=="E":
            a=1
        #Define la condición para saber si es ascendente o descendente
        if Orden[3]=="A":
            Cond=Matriz[j][a]>Matriz[j+1][a]
        if Orden[3]=="D":
            Cond=Matriz[j][a]<Matriz[j+1][a]
        if Cond:
            Tmp=Matriz[j]#Temporal:Guarda el valor que va a intercambiar
            Matriz[j]=Matriz[j+1]
            Matriz[j+1]=Tmp
            return orden_pilotos_aux(Matriz,i,j+1,n,True,Orden)
        else:
            return orden_pilotos_aux(Matriz,i,j+1,n,Swap,Orden)
##Ventana de edicion o de un piloto
    def editar_piloto(Piloto):
        """
        Función que crea una ventana de edición con Entries para editar cada dato, un botón de salir para y un boton de aceptar que llama a la función
        editar_piloto_aux. Los entries tienen por default la información actual del piloto, tiene como entrada al piloto que se quiere editar
        """
        global Editando
        if not Editando:
            Editando=True
            edicion=Toplevel()
            edicion.title("Editar piloto")
            edicion.minsize(500,500)
            C_edicion=Canvas(edicion,width=500,height=500)
            C_edicion.place(x=0,y=0)
            Editbck=loadImg("Editbck.gif")
            C_edicion.create_image(0,0,image=Editbck,anchor=NW, state=NORMAL)
            
            edicion.overrideredirect(1)
            
            Pilotos=obtener_pilotos()
            def editar_piloto_aux(Piloto,Pag):
                """
                Captura los datos de la pantalla de edición y reescribe la matriz con los nuevos datos
                Entradas: El piloto que se quiere editar, y la página que hay que refrescar cuando se reescribe la matriz
                Restricciones: El piloto es un número entero del 0 al 9 que representa el índice de la matriz, cada elemento es
                               un vector con los datos de cada piloto y la página es 1 o 2.
                Salidas: Se reescribe la matriz y se actualzan los datos de la página
                """
                global Editando
                Editando=False
                Matriz=obtener_pilotos()
                try:
                    
                    Imagen=str(E_imagen.get())
                    Nombre=str(E_nombre.get()[0:11])
                    Edad=int(E_edad.get()[0:2])
                    Nacion=str(E_pais.get()[0:5])
                    Temporada=str(E_temp.get()[0:5])
                    Competencias=int(E_comp.get()[0:3])
                    V=int(E_vic.get()[0:3])
                    P=int(E_p.get()[0:3])
                    A=int(E_aban.get()[0:3])
                    RGP=round(((V+P)/(Competencias-A))*100)
                    REP=round((V/(Competencias-A))*100)
                    try:
                        loadImg(Imagen)
                    except:
                        Imagen=Matriz[Piloto][2]
                        messagebox.showinfo("Error", "La imagen no existe")
                        
                    
                except:
                    messagebox.showinfo("Error", "Por favor llenar todos los espacios")
                    
                if V<=Competencias and P<=Competencias and A<=Competencias and A+V+P<=Competencias:
                    VecPilotos=[RGP,REP,Imagen,Nombre,Edad,Nacion,Temporada,Competencias,V,P,A]
                    Matriz[Piloto]=VecPilotos
                    reescribir(Matriz,"Pilotos.txt")
                    edicion.destroy()
                    place_pilots(obtener_pilotos(),"destroy",Pag)
                    place_pilots(obtener_pilotos(),"write",Pag)

                else:
                    messagebox.showinfo("Error", "Los datos de las competencias son inválidos")

          
            Label(C_edicion, text="Editar piloto #"+str(Piloto+1),font=("Steamwreck",20),fg="white",bg="black").place(x=5,y=5)
            Aceptar=Button(C_edicion, command=lambda:editar_piloto_aux(Piloto,Pag), text="Ok",font=("Steamwreck",20),fg="white",bg="black")
            Aceptar.place(x=400,y=440)
            Quit= Button(C_edicion, command=lambda:salir(edicion),text="Salir",font=("Steamwreck",20),fg="white",bg="black")
            Quit.place(x=10,y=440)

            Posy=45
            for Elemento in ["Nombre (Max:15 char)","Edad (0 al 99)","Pais(Max:5 char)","Temporada (Max:5 char)", "Competencias (0 al 100)","Victorias","Segundo o tercer lugar","Cantidad de abandonos","Nombre de la foto"]:
                L_Titulos=Label(C_edicion,text=Elemento,font=("Steamwreck",20),fg="white",bg="black")
                L_Titulos.place(x=10,y=Posy)
                Posy+=45

            #Entry de la ventana de edición de los pilotos
            E_nombre=Entry(C_edicion,width=30,font=("Steamwreck",20))
            E_nombre.place(x=200,y=45)
            E_nombre.insert(END,obtener_pilotos()[Piloto][3])#Función que ingresa un dato default al entry
            E_edad=Entry(C_edicion,width=30,font=("Steamwreck",20))
            E_edad.place(x=200,y=90)
            E_edad.insert(END,obtener_pilotos()[Piloto][4])
            E_pais=Entry(C_edicion,width=30,font=("Steamwreck",20))
            E_pais.place(x=200,y=135)
            E_pais.insert(END,obtener_pilotos()[Piloto][5])
            E_temp=Entry(C_edicion,width=30,font=("Steamwreck",20))
            E_temp.place(x=200,y=180)
            E_temp.insert(END,obtener_pilotos()[Piloto][6])
            E_comp=Entry(C_edicion,width=30,font=("Steamwreck",20))
            E_comp.place(x=200,y=225)
            E_comp.insert(END,obtener_pilotos()[Piloto][7])
            E_vic=Entry(C_edicion,width=30,font=("Steamwreck",20))
            E_vic.place(x=200,y=270)
            E_vic.insert(END,obtener_pilotos()[Piloto][8])
            E_p=Entry(C_edicion,width=30,font=("Steamwreck",20))
            E_p.place(x=200,y=315)
            E_p.insert(END,obtener_pilotos()[Piloto][9])
            E_aban=Entry(C_edicion,width=30,font=("Steamwreck",20))
            E_aban.place(x=200,y=360)
            E_aban.insert(END,obtener_pilotos()[Piloto][10])
            E_imagen=Entry(C_edicion,width=30,font=("Steamwreck",20))
            E_imagen.place(x=200,y=405)
            E_imagen.insert(END,obtener_pilotos()[Piloto][2])
        else:
            messagebox.showinfo("Error", "Ya se está editando un piloto")
        edicion.mainloop()
            


    def ventana_carros(Pag):
        posiciones.destroy()
        ventana_carros_aux(Pag)
    #Botones de la ventana de posiciones
        #Botones de orden
    BRGPA=Button(C_posiciones, command=lambda: orden_pilotos(obtener_pilotos(),"RGPA",Pag),font=("Steamwreck",20),text="RGP(Ascendente)",fg="black",bg="white")
    BRGPA.place(x=450,y=0)
    BRGPD=Button(C_posiciones, command=lambda: orden_pilotos(obtener_pilotos(),"RGPD",Pag),font=("Steamwreck",20),text="RGP(Descendente)",fg="black",bg="white")
    BRGPD.place(x=600,y=0)
    BREPA=Button(C_posiciones, command=lambda: orden_pilotos(obtener_pilotos(),"REPA",Pag),font=("Steamwreck",20),text="REP(Ascendente)",fg="black",bg="white")
    BREPA.place(x=900,y=0)
    BREPD=Button(C_posiciones, command=lambda: orden_pilotos(obtener_pilotos(),"REPD",Pag),font=("Steamwreck",20),text="REP(Descendente)",fg="black",bg="white")
    BREPD.place(x=1050,y=0)
        #Boton de reset y de volver a la principal
    BBack=Button(C_posiciones, command=lambda: back(posiciones), text="Back",font=("Steamwreck",20), fg="white",bg="black")
    BBack.place(x=0,y=60)
    BCarros=Button(C_posiciones,command=lambda:ventana_carros(1), text="Carros", font=("Steamwreck",20), fg="black",bg="white")
    BCarros.place(x=0,y=0)
    
        #Botones de edicion de pilotos
    Edit=loadImg("Edit.gif")
    Edit1=Button(C_posiciones, command=lambda:editar_piloto(P) ,bg="black",image=Edit)
    Edit1.place(x=0,y=155)
    Edit2=Button(C_posiciones, command=lambda:editar_piloto(P+1) ,bg="black",image=Edit)
    Edit2.place(x=0,y=275)
    Edit3=Button(C_posiciones, command=lambda:editar_piloto(P+2) ,bg="black",image=Edit)
    Edit3.place(x=0,y=385)
    Edit4=Button(C_posiciones, command=lambda:editar_piloto(P+3) ,bg="black",image=Edit)
    Edit4.place(x=0,y=515)
    Edit5=Button(C_posiciones, command=lambda:editar_piloto(P+4) ,bg="black",image=Edit)
    Edit5.place(x=0,y=625)
    def seleccionar_piloto(P):
        #Cambia la variable global piloto dependiendo del que se seleccione
        global Piloto
        Piloto=P
        print(Piloto)
    Selec=loadImg("Selec.gif")
    Selec1=Button(C_posiciones, command=lambda:seleccionar_piloto(P) ,bg="#332904",image=Selec)
    Selec1.place(x=0,y=185)
    Selec2=Button(C_posiciones, command=lambda:seleccionar_piloto(P+1) ,bg="#332904",image=Selec)
    Selec2.place(x=0,y=305)
    Selec3=Button(C_posiciones, command=lambda:seleccionar_piloto(P+2) ,bg="#332904",image=Selec)
    Selec3.place(x=0,y=415)
    Selec4=Button(C_posiciones, command=lambda:seleccionar_piloto(P+3) ,bg="#332904",image=Selec)
    Selec4.place(x=0,y=545)
    Selec5=Button(C_posiciones, command=lambda:seleccionar_piloto(P+4) ,bg="#332904",image=Selec)
    Selec5.place(x=0,y=655)

    def paginapos1():
        place_pilots(obtener_pilotos(),"destroy",1)
        place_pilots(obtener_pilotos(),"write",1)
    def paginapos2():
        place_pilots(obtener_pilotos(),"destroy",2)
        place_pilots(obtener_pilotos(),"write",2)
    BPag1=Button(C_posiciones, command=paginapos1 ,font=("Steamwreck",20),text="Pag1",fg="white",bg=ColorPag1)
    BPag1.place(x=120,y=0)
    BPag2=Button(C_posiciones, command=paginapos2 ,font=("Steamwreck",20),text="Pag2",fg="white",bg=ColorPag2)
    BPag2.place(x=180,y=0)
    place_pilots(obtener_pilotos(),"write",Pag)
    posiciones.mainloop()
#_______________________________                                __________________________________________________
#                               \Fin de la página de posiciones/



#_____________________________/Inicio de la ventana de posiciones de carros\______________________________________
def ventana_carros_aux(Pag):
    """

    Función que crea la ventana de posiciones de los vehículos, recibe el número de páginas por que son 10 vehículos
    y muestra 5 en cada página, tiene un botón para volver a la pantalla principal, 2 botones para ordenar los
    pilotos dependiendo del que se seleccione (según eficiencia ya sea ascendente o descendente y un boton para ir
    a la ventana que muestra los pilotos
    """
    carros=Toplevel()
    carros.title("Carros")
    carros.minsize(1280,720)
    C_carros=Canvas(carros, width=1280, height=720, bg="dark green")
    C_carros.place(x=0,y=0)

    #Imagense de fondo
    Carrosbck= loadImg("Carrosbck.gif")
    C_carros.create_image(0,0, image=Carrosbck, anchor=NW,state=HIDDEN, tags="Pag1")
    Carrosbck2= loadImg("Carrosbck2.gif")
    C_carros.create_image(0,0, image=Carrosbck2, anchor=NW,state=HIDDEN, tags="Pag2")
    #Imagenes de pagina 1
    Carro1=loadImg(obtener_carros()[0][0])
    C_carros.create_image(125,130, image=Carro1, anchor=NW,state=HIDDEN,tags="Pag1")
    Carro2=loadImg(obtener_carros()[1][0])
    C_carros.create_image(125,250, image=Carro2, anchor=NW,state=HIDDEN,tags="Pag1")
    Carro3=loadImg(obtener_carros()[2][0])
    C_carros.create_image(125,370, image=Carro3, anchor=NW,state=HIDDEN,tags="Pag1")
    Carro4=loadImg(obtener_carros()[3][0])
    C_carros.create_image(125,490, image=Carro4, anchor=NW,state=HIDDEN,tags="Pag1")
    Carro5=loadImg(obtener_carros()[4][0])
    C_carros.create_image(125,610, image=Carro5, anchor=NW,state=HIDDEN,tags="Pag1")
    #Imagenes de pagina 2
    Carro6=loadImg(obtener_carros()[5][0])
    C_carros.create_image(125,130, image=Carro6, anchor=NW,state=HIDDEN,tags="Pag2")
    Carro7=loadImg(obtener_carros()[6][0])
    C_carros.create_image(125,250, image=Carro7, anchor=NW,state=HIDDEN,tags="Pag2")
    Carro8=loadImg(obtener_carros()[7][0])
    C_carros.create_image(125,370, image=Carro8, anchor=NW,state=HIDDEN,tags="Pag2")
    Carro9=loadImg(obtener_carros()[8][0])
    C_carros.create_image(125,490, image=Carro9, anchor=NW,state=HIDDEN,tags="Pag2")
    Carro10=loadImg(obtener_carros()[9][0])
    C_carros.create_image(125,610, image=Carro10, anchor=NW,state=HIDDEN,tags="Pag2")

    def orden_carros(Matriz,Orden,Pag):
        """
        Función que ordena a los carros dependiendo de como se requiera llamando a la función orden_carros_aux
        Entradas: Una matriz,con las datos de los vehículos, el orden respectivo y la página que hay que destruir y escribir para refrescar
        Restricciones: El último dato de cada elemento de la matriz es el rendimiento del vehículo, la página puede ser 1 o 2 y el orden puede ser:
                                -"Ascendente"
                                -"Descendente"
        Salida: Se reescribe la matriz en el archivo de carros y se destruye la ventana y se vuelve a crear con los datos actualizados
        """
        reescribir(orden_carros_aux(Matriz,0,0,len(Matriz),False,Orden),"Carros.txt")
        place_carros(obtener_carros(),"destroy",Pag)
        place_carros(obtener_carros(),"write",Pag)
        
    def orden_carros_aux(Matriz,i,j,n,Swap,Orden):
        """
        Ordena los datos de la matriz según el orden que se le asigne por medio del algoritmo de ordenamiento de burbuja, es la función auxiliar
        de la funcion orden_carros
        """
        if j==n-i-1:
            if Swap:
                return orden_carros_aux(Matriz,i+1,0,n,False,Orden)
            else:
                return Matriz
        if Orden=="A":
            Cond=Matriz[j][4]>Matriz[j+1][4]
        if Orden=="D":
            Cond=Matriz[j][4]<Matriz[j+1][4]
        if Cond:
            Tmp=Matriz[j]#Temporal:Guarda el valor que va a intercambiar
            Matriz[j]=Matriz[j+1]
            Matriz[j+1]=Tmp
            return orden_carros_aux(Matriz,i,j+1,n,True,Orden)
        else:
            return orden_carros_aux(Matriz,i,j+1,n,Swap,Orden)
    def place_carros(Matriz,Accion,Pagina):
        """
        Función que coloca a los vehículos es las tablas de posiciones
        Entradas: La matriz a colocar, en este caso la de los vehículos
                  La acción, es decir si "write" escribe a los pilotos o "destroy" destruye la ventana para escribir algo nuevo
                  La página en que se está, con esto se decide el rango de la matriz, es decir de vehículos que se escriben
        Restricciones: Debe entrar una matriz, la acción puede ser "write" o "destroy" y la página solo puede se 1 o 2
        Salida: Se posicionan los datos de los vehículos en la ventana
        """
        if Pagina==1:
            C_carros.itemconfig("Pag2",state=HIDDEN)
            C_carros.itemconfig("Pag1",state=NORMAL)
            Carros=Matriz[0:5]
        if Pagina==2:
            C_carros.itemconfig("Pag1",state=HIDDEN)
            C_carros.itemconfig("Pag2",state=NORMAL)
            Carros=Matriz[5:10]
        if Accion=="write":
            Posy=145
            for Car in Carros:
                Posx=120
                
                for Ind in range(0,5):
                    if Ind==0:
                        Posx=-400
                    if Ind==1:
                        Posx=425
                    if Ind==2:
                        Posx=700
                    if Ind==3:
                        Posx=970
                    if Ind==4:
                        Posx=1140

                    L_Carros=Label(C_carros,text=str(Car[Ind]),font=("Steamwreck",30),fg="white",bg="black")
                    L_Carros.place(x=Posx,y=Posy)
                Posy+=120
        if Accion=="destroy":
            carros.destroy()
            ventana_carros_aux(Pagina)
    def editar_carro(Carro):
        """
        Función que crea una ventana de edición con Entries para editar cada dato, un botón de salir para y un boton de aceptar que llama a la función
        editar_carro_aux. Los entries tienen por default la información actual del vehículo, tiene como entrada al vehículo que se quiere editar
        """
        global Editando
        if not Editando:
            Editando=True
            edicioncar=Toplevel()
            edicioncar.title("Editar carro")
            edicioncar.minsize(500,500)
            C_edicioncar=Canvas(edicioncar,width=500,height=500,bg="black")
            C_edicioncar.place(x=0,y=0)
            Editbck2=loadImg("Editbck2.gif")
            C_edicioncar.create_image(0,0,anchor=NW,image=Editbck2)
            edicioncar.overrideredirect(1)
            
            def editar_carro_aux(Carro,Pag):
                """
                Captura los datos de la pantalla de edición y reescribe la matriz con los nuevos datos
                Entradas: El vehículo que se quiere editar, y la página que hay que refrescar cuando se reescribe la matriz
                Restricciones: El vehículo es un número entero del 0 al 9 que representa el índice de la matriz, cada elemento es
                               un vector con los datos de cada vehículo y la página es 1 o 2.
                Salidas: Se reescribe la matriz y se actualzan los datos de la página
                """
                global Editando
                Editando=False
                Carros=obtener_carros()
                try:
                    
                    Marca=str(E_marca.get()[0:21])
                    Modelo=str(E_modelo.get()[0:21])
                    Temporada=str(E_temporada.get()[0:5])
                    Eficiencia=float(E_eficiencia.get()[0:4])
                    Imagen=str(E_imagen.get())
                    try:
                        loadImg(Imagen)
                    except:
                        Imagen=Carros[Carro][0]
                        messagebox.showinfo("Error", "La imagen no existe")
                    
                    VecCarros=[Imagen,Marca,Modelo,Temporada,Eficiencia]
                    Carros[Carro]=VecCarros
                    reescribir(Carros,"Carros.txt")
                    edicioncar.destroy()
                    place_carros(obtener_carros(),"destroy",Pag)
                    place_carros(obtener_carros(),"write",Pag)
                except:
                    messagebox.showinfo("Error", "Por favor llenar todos los espacios")
                    
                
                
                
            Label(C_edicioncar, text="Editar Carro #"+str(Carro+1),font=("Steamwreck",25),fg="white",bg="black").place(x=150,y=5)
            Aceptar=Button(C_edicioncar, command=lambda:editar_carro_aux(Carro,Pag), text="Ok",font=("Steamwreck",20),fg="white",bg="black")
            Aceptar.place(x=400,y=440)
            Quit= Button(C_edicioncar, command=lambda:salir(edicioncar),text="Salir",font=("Steamwreck",20),fg="white",bg="black")
            Quit.place(x=10,y=440)
            Posy=60
            #Para posicionar las etiquetas a la par de cada entry
            for Elemento in ["Marca (Max:20 char)","Modelo (Max: 20 char)","Temporada(Max:4 char)","Eficiencia (Max:3 char)","Nombre de imagen:"]:
                L_Titulos=Label(C_edicioncar,text=Elemento,font=("Steamwreck",20),fg="white",bg="black")
                L_Titulos.place(x=10,y=Posy)
                Posy+=75

            #Entry de la ventana de edición
            E_marca=Entry(C_edicioncar,width=30,font=("Steamwreck",20))
            E_marca.place(x=200,y=60)
            E_marca.insert(END,obtener_carros()[Carro][1])
            E_modelo=Entry(C_edicioncar,width=30,font=("Steamwreck",20))
            E_modelo.place(x=200,y=135)
            E_modelo.insert(END,obtener_carros()[Carro][2])
            E_temporada=Entry(C_edicioncar,width=30,font=("Steamwreck",20))
            E_temporada.place(x=200,y=219)
            E_temporada.insert(END,obtener_carros()[Carro][3])
            E_eficiencia=Entry(C_edicioncar,width=30,font=("Steamwreck",20))
            E_eficiencia.place(x=200,y=285)
            E_eficiencia.insert(END,obtener_carros()[Carro][4])
            E_imagen=Entry(C_edicioncar,width=30,font=("Steamwreck",20))
            E_imagen.place(x=200,y=350)
            E_imagen.insert(END,obtener_carros()[Carro][0])
            
        else:
            messagebox.showinfo("Error", "Ya se está editando un carro")
        edicioncar.mainloop()
        
    #Función para volver a la pantalla de pilotos
    def volver_a_pilotos():
        carros.destroy()
        ventana_posiciones(1)

    #Escoge el índice que envía a la pantalla de editar o seleccionar dependiendo de la página, el color es para que los botones cambien
    #dependiendo de la página actual
    if Pag==1:
        ColorPag1="#332904"
        ColorPag2="black"        
        Carro=0
    if Pag==2:
        ColorPag1="black"
        ColorPag2="#332904"
        Carro=5
    place_carros(obtener_carros(),"write",Pag)
    BPilotos=Button(C_carros,command=volver_a_pilotos, text="Pilotos", font=("Steamwreck",20), fg="black",bg="white")
    BPilotos.place(x=0,y=0)
    BBack=Button(C_carros, command=lambda: back(carros), text="Back",font=("Steamwreck",20), fg="white",bg="black")
    BBack.place(x=0,y=60)
    #Botones de edición
    Edit=loadImg("Edit.gif")
    Edit1=Button(C_carros, command=lambda:editar_carro(Carro) ,bg="black",image=Edit)
    Edit1.place(x=0,y=155)
    Edit2=Button(C_carros, command=lambda:editar_carro(Carro+1) ,bg="black",image=Edit)
    Edit2.place(x=0,y=275)
    Edit3=Button(C_carros, command=lambda:editar_carro(Carro+2) ,bg="black",image=Edit)
    Edit3.place(x=0,y=385)
    Edit4=Button(C_carros, command=lambda:editar_carro(Carro+3) ,bg="black",image=Edit)
    Edit4.place(x=0,y=515)
    Edit5=Button(C_carros, command=lambda:editar_carro(Carro+4) ,bg="black",image=Edit)
    Edit5.place(x=0,y=625)
    def seleccionar_carro(Car):
        if obtener_carros()[Car][0]=="Carro10.gif":
            if obtener_bateria()<60:
                messagebox.showinfo("Error", "El vehículo está descargado")
            else:
                pass
        else:
            messagebox.showinfo("Error", "Este vehículo no está disponible")
        

        
    Selec=loadImg("Selec.gif")   
    Selec1=Button(C_carros, command=lambda:seleccionar_carro(Carro) ,bg="#332904",image=Selec)
    Selec1.place(x=0,y=185)
    Selec2=Button(C_carros, command=lambda:seleccionar_carro(Carro+1) ,bg="#332904",image=Selec)
    Selec2.place(x=0,y=305)
    Selec3=Button(C_carros, command=lambda:seleccionar_carro(Carro+2) ,bg="#332904",image=Selec)
    Selec3.place(x=0,y=415)
    Selec4=Button(C_carros, command=lambda:seleccionar_carro(Carro+3) ,bg="#332904",image=Selec)
    Selec4.place(x=0,y=545)
    Selec4=Button(C_carros, command=lambda:seleccionar_carro(Carro+4) ,bg="#332904",image=Selec)
    Selec4.place(x=0,y=655)
    


    BRendA=Button(C_carros, command=lambda: orden_carros(obtener_carros(),"A",Pag),font=("Steamwreck",20),text="Ascendente",fg="black",bg="white")
    BRendA.place(x=1050,y=0)
    BRendD=Button(C_carros, command=lambda: orden_carros(obtener_carros(),"D",Pag),font=("Steamwreck",20),text="Descedente",fg="black",bg="white")
    BRendD.place(x=1150,y=0)
    def pagina1():
        place_carros(obtener_carros(),"destroy",1)
        place_carros(obtener_carros(),"write",1)
    def pagina2():
        place_carros(obtener_carros(),"destroy",2)
        place_carros(obtener_carros(),"write",2)
    BPag1=Button(C_carros, command=pagina1 ,font=("Steamwreck",20),text="Pag1",fg="white",bg=ColorPag1)
    BPag1.place(x=120,y=0)
    BPag2=Button(C_carros, command=pagina2 ,font=("Steamwreck",20),text="Pag2",fg="white",bg=ColorPag2)
    BPag2.place(x=180,y=0)
    carros.mainloop()



def obtener_pilotos():
    """
    Función que lee el archivo Pilotos.txt y retorna la matriz escrita con todos los datos de los 10 pilotos 
    """
    Archivo=open("Pilotos.txt","r+")
    Archivo.seek(0)
    Matriz= ast.literal_eval(Archivo.read())
    Archivo.close()
    return Matriz
def reescribir(DatoMatriz,Texto):
    """
    Reescribe la matriz que le entra al documento
    Entradas: MatrizPilotos, es la matriz que se va a escribir y Texto es en el arhivo que se va a escribir
    Restricciones: Texto debe ser "Pilotos.txt", "Carros.txt" o "Bateria.txt"
    Salida: Se reescribe la nueva matriz o dato en el documento
    """
    Archivo=open(Texto,"r+")
    Archivo.seek(0)
    Archivo.truncate()
    Archivo.write(str(DatoMatriz))
    Archivo.close()
    
def obtener_carros():
    """
    Función que lee el archivo Carros.txt y retorna la matriz escrita con todos los datos de los 10 vehículos  
    """
    Archivo=open("Carros.txt","r+")
    Archivo.seek(0)
    Matriz= ast.literal_eval(Archivo.read())
    Archivo.close()
    return Matriz

def obtener_bateria():
    """
    Función para obtener el último valor de batería guardado
    """
    Archivo=open("Bateria.txt","r+")
    Archivo.seek(0)
    Dato=int(Archivo.read())
    Archivo.close()
    return Dato
def obtener_main():
    """
    Función que retorna una lista con las rutas de las imágenes configuradas en la pantalla principal del logo y los patrocinadores
    """
    Archivo=open("Mainrutas.txt","r+")
    Archivo.seek(0)
    Dato=ast.literal_eval(Archivo.read())
    Archivo.close()
    return Dato

def available_car():
    Mat=obtener_carros()
    for Car in Mat:
        if Car[0]=="Carro10.gif":
            return Car
        else:
            pass

def IGE():
    """
    Función que calcula el índice ganador de escudería y lo retorna en forma de string para crear un texto en la pantalla
    """
    Matriz=obtener_pilotos()
    V=0
    T=0
    for Piloto in Matriz:
        V+=Piloto[8]
        T+=Piloto[7]
    return str(round(V/T*100))+"%"
    
def refresh_IGE():
    """
    Función ligada a un botón para actualizar el índice ganador de escudería
    """
    C_main.itemconfig("IGE",text=IGE())        
        
    
    
#----------VENTANA DEL ABOUT---------------
def about():
    """
    Creación de la ventana de about, crea una ventana con la información de los programadores y la foto y otros datos
    de interés como la institución, versión del programa, etc.
    """
    main.withdraw()
    about=Toplevel()
    about.title("Acerca de")
    about.minsize(1280,720)
   
    C_about=Canvas(about, width=1280,height=720, bg="black")
    C_about.place(x=0,y=0)
    
    Aboutbck= loadImg("Aboutbck.gif")
    C_about.create_image(0,0, image=Aboutbck, anchor=NW,state=NORMAL)

    Nati=loadImg("Nati.gif")
    Nati2=loadImg("Nati2.gif")
    Jose=loadImg("Jose.gif")
    Jose2=loadImg("Jose2.gif")
    C_about.create_image(500,120,image=Jose,anchor=NW,state=NORMAL,tags="Fotos1")
    C_about.create_image(900,120,image=Nati,anchor=NW,state=NORMAL,tags="Fotos1")
    C_about.create_image(500,120,image=Jose2,anchor=NW,state=HIDDEN,tags="Fotos2")
    C_about.create_image(900,120,image=Nati2,anchor=NW,state=HIDDEN,tags="Fotos2")
    
    def idk(event):
        C_about.itemconfig("Fotos1",state=HIDDEN)
        C_about.itemconfig("Fotos2",state=NORMAL)
    def idk2(event):
        C_about.itemconfig("Fotos2",state=HIDDEN)
        C_about.itemconfig("Fotos1",state=NORMAL)

    
    about.bind("<Button-3>",idk)
    about.bind('<ButtonRelease-3>',idk2)
    
    LAbout=Label(C_about, text=About, font=("Steamwreck",25),fg="white",bg="#120f01")
    LAbout.place(x=20,y=0)
    BBack= Button(C_about, command=lambda: back(about),text="Back",font=("Steamwreck",25), fg="black", bg="white")
    BBack.place(x=10,y=650)

    
    about.mainloop()

#Funcion para volver a la pantalla principal, destruye la ventana y reaparece la principal
def back(Ventana):
    Ventana.destroy()
    main.deiconify()
#Funcion para destruir pantallas de edición
def salir(Ventana):
    global Editando
    Ventana.destroy()
    Editando=False

def ventana_conducir():
    """
    Función para ingresar a la pantalla de Test Drive, si la batería es menor a 60% sale un mensaje de que el auto no está disponible, después manda
    un mensaje si se quiere continuar y si se responde que si, se abre la ventana de Test Drive con el piloto seleccionado
    """
    global Piloto
    if obtener_bateria()<60:
        messagebox.showinfo("Aviso!", "El vehiculo esta descargado! \nPor favor recargue las baterias.")
    yesno = messagebox.askyesno("Confirmar", "Es ud la señora Nati o el amo Jose?")
    if not (yesno):
        pass
    else:
        main.withdraw()
        TestDrive.test_drive(Piloto,obtener_pilotos()[Piloto],main,obtener_pilotos(),obtener_main()[0])


def editar_sponsors(Ind,Tag):
    """
    Función que crea una ventana de edición con un Entry para editar el nombre de la imagen, un botón de salir y un boton de aceptar que llama a la función
    editar_sponsor_aux. El Entry tiene por default la instrucción de lo que tiene que escribir, tiene como entrada el tag de la imagen que se quiere editar
    y el índice de la lista donde se debe guardar el nombre 
    """
    global Editando
    if not Editando:
            Editando=True
            edicions=Toplevel()
            edicions.title("Editar Sponsor")
            edicions.minsize(500,10)
            edicions.overrideredirect(1)
            C_edicions=Canvas(edicions,width=500,height=500, bg="black")
            C_edicions.place(x=0,y=0)
            E_imagen=Entry(edicions,width=100,font=("Steamwreck",20),fg="gray")
            E_imagen.place(x=0,y=0)
            E_imagen.insert(END,"Escriba el nombre de la imagen de la nueva imagen")
            
            def editar_sponsor_aux(Ind,Tag):
                """
                Captura los datos de la pantalla de edición y reescribe la lista con la nueva ruta de la imagen
                Entradas: El tag de la imagen que se quiere editar y el índice de la lista en que se encuentra el nombre de la imagen
                Restricciones: El índice es de 0 a 3 y los tags solo de la imagen que se quiere editar y el nombre de la imagen debe existir
                Salidas: Se reescribe la la lista y se configura la imagen con la nueva
                """
                global Editando
                Img=str(E_imagen.get())
                try:  #En caso que la imagen no exista  
                    Editando=False
                    Imagen=loadImg(Img)
                    C_main.itemconfig(Tag,image=Imagen)
                    edicions.destroy()
                    Lista=obtener_main()
                    Lista[Ind]=Img
                    reescribir(Lista,"Mainrutas.txt")
                    
                    main.mainloop()
                   
                except:
                    messagebox.showinfo("ERROR", "No se puede cargar la imagen") 

            Aceptar=Button(edicions, command=lambda:editar_sponsor_aux(Ind,Tag), text="Aceptar",font=("Steamwreck",20),fg="white",bg="black")
            Aceptar.place(x=300,y=100)
            Quit= Button(edicions, command=lambda:salir(edicions),text="Cancelar",font=("Steamwreck",20),fg="white",bg="black")
            Quit.place(x=20,y=100)
            
            edicions.mainloop()
#Imagen del logo    
Logo_name=obtener_main()[0]
Logo=loadImg(Logo_name)
C_main.create_image(0,0,image=Logo,anchor=NW,tags="Logo")

#Imagenes de sponsors
C_main.create_text(1150,0,text="Sponsored by:", font=("Steamwreck",30),fill="white",anchor=NE)
Sponsor1=loadImg(obtener_main()[1])
C_main.create_image(1180,50,image=Sponsor1,anchor=NE,tags="Spon1")
Sponsor2=loadImg(obtener_main()[2])
C_main.create_image(1180,260,image=Sponsor2,anchor=NE,tags="Spon2")
Sponsor3=loadImg(obtener_main()[3])
C_main.create_image(1180,400,image=Sponsor3,anchor=NE,tags="Spon3")
#Botones para editar las imágenes de la pantalla principal
Edit=loadImg("Edit2.gif")
Edit0=Button(C_main, command=lambda:editar_sponsors(0,"Logo") ,bg="black",image=Edit)
Edit0.place(x=0,y=0)
Edit1=Button(C_main, command=lambda:editar_sponsors(1,"Spon1") ,bg="black",image=Edit)
Edit1.place(x=1180,y=150)
Edit2=Button(C_main, command=lambda:editar_sponsors(2,"Spon2") ,bg="black",image=Edit)
Edit2.place(x=1180,y=360)
Edit3=Button(C_main, command=lambda:editar_sponsors(3,"Spon3") ,bg="black",image=Edit)
Edit3.place(x=1180,y=500)

BAbout=Button(C_main, command=about, text="Acerca del programa",font=("Steamwreck",25), fg="#f4d88e",bg="#261d11")
BAbout.place(x=380,y=600)
BPosiciones=Button(C_main, command=lambda:ventana_posiciones(1), text="Tabla de Posiciones",font=("Steamwreck",25), fg="#f4d88e",bg="#261d11")
BPosiciones.place(x=10,y=600)
BTestDrive=Button(C_main, command=ventana_conducir, text="Test Drive",font=("Steamwreck",35), fg="#f4d88e",bg="#261d11")
BTestDrive.place(x=200,y=400)
BIGE=Button(C_main, command=refresh_IGE, text="Indice Ganador de Escuderia:", font=("Steamwreck",20), fg="#261d11",bg="#f4d88e").place(x=600,y=607)
C_main.create_text(860,630,font=("Steamwreck",40),text=IGE(), fill="#f4d88e", tags="IGE")
C_main.create_text(1280,720,font=("Steamwreck",40),text="Temporada 2019", fill="#f4d88e",anchor=SE)





main.mainloop()

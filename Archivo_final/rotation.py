"""
Copyright (C) 2018 FireEye, Inc., created by Andrew Shay. All Rights Reserved.
"""

import PIL

from PIL import Image


def rotate(porcentaje):
    percent = ((porcentaje-700)/324)*100  # Percent for gauge
    output_file_name = 'new_gauge.png'

    # X and Y coordinates of the center bottom of the needle starting from the top left corner
    #   of the image
    x = 255
    y = 301
    loc = (x, y)

    percent = percent / 100
    rotation = 210 * percent  # 180 degrees because the gauge is half a circle
    rotation = 90 - rotation  # Factor in the needle graphic pointing to 50 (90 degrees)

    dial = Image.open('needle7.png')
    dial = dial.rotate(rotation, resample=PIL.Image.BICUBIC, center=loc,)  # Rotate needle

    gauge = Image.open('gauge.png')
    gauge.paste(dial, mask=dial)  # Paste needle onto gauge
    gauge.save(output_file_name)
